<!-- SUB BANNER -->
<section class="section-sub-banner awe-parallax bg-9">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2>Liên hệ</h2>
            </div>
        </div>

    </div>

</section>
<!-- END / SUB BANNER -->

<!-- CONTACT -->
<section class="section-contact">
    <div class="container">
        <div class="contact">
            <div class="row">

                <div class="col-md-6 col-lg-5">

                    <div class="text">
                        <h2>Liên hệ</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                        <?php
                        foreach ($ks as $key=>$value){
                        ?>
                        <ul>
                            <li><i class="icon hillter-icon-location"></i>  <?php echo $value->dia_chi?></li>
                            <li><i class="icon hillter-icon-phone"></i> <?php echo $value->so_dien_thoai?></li>
                            <li><i class="icon fa fa-envelope-o"></i> <a><?php echo $value->email?></a></li>
                        </ul>
                        <?php
                        }
                        ?>
                    </div>


                </div>

                <div class="col-md-6 col-lg-6 col-lg-offset-1">
                    <div class="contact-map">
                        <div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.9022670529275!2d105.78571391415758!3d21.036596192888403!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab49fe1e3eab%3A0x7fcf371aff552606!2zNjggWHXDom4gVGjhu6d5LCBE4buLY2ggVuG7jW5nIEjhuq11LCBD4bqndSBHaeG6pXksIEjDoCBO4buZaSAxMjMxODEsIFZp4buHdCBOYW0!5e0!3m2!1svi!2sus!4v1628331617392!5m2!1svi!2sus" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- END / CONTACT -->
