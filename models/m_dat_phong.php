<?php
require_once ('database.php');
class m_dat_phong extends database
{
    public function insert_dat_phongs($id, $ten_khach_hang, $so_dien_thoai, $CMND, $email, $id_phong, $gia, $id_phan_tram_km, $so_luong, $ngay_den, $ngay_di, $nguoi_lon, $tre_em, $thanh_tien, $trang_thai)
    {
        $sql = "insert into dat_phong values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id, $ten_khach_hang, $so_dien_thoai, $CMND, $email, $id_phong, $gia, $id_phan_tram_km, $so_luong, $ngay_den, $ngay_di, $nguoi_lon, $tre_em, $thanh_tien, $trang_thai));
    }
}