<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" align="center">
<!--        <span class="brand-text font-weight-light">Welcome to Lucy Hotel</span>-->
        <img src="../public/images/photos/Picture1.png" width="200" height="100">
    </a>

    <!-- Sidebar -->


    <!-- SidebarSearch Form -->

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="khach_san.php" class="nav-link">
                    <i class="nav-icon fa fa-hotel"></i>
                    <p>
                        khách sạn
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="nguoi_dung.php" class="nav-link">
                    <i class="nav-icon fa fa-id-badge"></i>
                    <p>
                        Người dùng
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="loai_nguoi_dung.php" class="nav-link">
                    <i class="nav-icon fa fa-id-badge"></i>
                    <p>
                        Loại người dùng
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="loai_phong.php" class="nav-link">
                    <i class="nav-icon fa fa-layer-group"></i>
                    <p>
                        Loại phòng
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="phong.php" class="nav-link">
                    <i class="nav-icon fa fa-door-closed"></i>
                    <p>
                        Phòng
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="dat_phong.php" class="nav-link">
                    <i class="nav-icon fa fa-id-badge"></i>
                    <p>
                        Danh sách đặt phòng
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="khach_hang.php" class="nav-link">
                    <i class="nav-icon fa fa-users"></i>
                    <p>
                        Khách hàng
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="dich_vu.php" class="nav-link">
                    <i class="nav-icon fa fa-scroll"></i>
                    <p>
                        Dịch vụ
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="bai_viet.php" class="nav-link">
                    <i class="nav-icon fa fa-scroll"></i>
                    <p>
                        Bài viết
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="banner.php" class="nav-link">
                    <i class="nav-icon fa fa-scroll"></i>
                    <p>
                        Banner
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="khuyen_mai.php" class="nav-link ">
                    <i class="nav-icon fa fa-tags"></i>
                    <p>
                        Khuyến mãi
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="hinh_thuc_thanh_toan.php" class="nav-link">
                    <i class="nav-icon fa fa-tags"></i>
                    <p>
                        Hình thức thanh toán
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="tin_tuc.php" class="nav-link">
                    <i class="nav-icon fa fa-scroll"></i>
                    <p>
                        Tin tức
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="hom_thu.php" class="nav-link active">
                    <i class="nav-icon fa fa-tags"></i>
                    <p>
                        Hòm thư
                    </p>
                </a>
            </li>
    </nav>
</aside>