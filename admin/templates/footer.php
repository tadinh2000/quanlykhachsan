<footer class="main-footer">
    <strong>Phần mềm quản lý khách sạn <a href="http://localhost/QuanLyKhachSan/index.php">Lucy Hotel</a>.</strong>

    <div class="float-right d-none d-sm-inline-block">
        <p class="float-right">Date: <?php echo $ndk = date("d/m/Y");?></p>
    </div>
</footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script src="public/layout/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="public/layout/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="public/layout/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="public/layout/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="public/layout/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="public/layout/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="public/layout/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="public/layout/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="public/layout/plugins/jszip/jszip.min.js"></script>
<script src="public/layout/plugins/pdfmake/pdfmake.min.js"></script>
<script src="public/layout/plugins/pdfmake/vfs_fonts.js"></script>
<script src="public/layout/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="public/layout/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="public/layout/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="public/layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="public/layout/dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
    $(function () {
        $("#example1").DataTable({
            "responsive": true, "lengthChange": false, "autoWidth": false,
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    });
    $(function () {
        bsCustomFileInput.init();
    });
</script>

<!-- jQuery -->
</body>
</html>