-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 10, 2021 lúc 06:37 PM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `quan_ly_khach_san`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bai_viet`
--

CREATE TABLE `bai_viet` (
  `id` int(10) NOT NULL,
  `hinh` varchar(255) NOT NULL,
  `ten_bai_viet` varchar(255) NOT NULL,
  `noi_dung` text NOT NULL,
  `id_khach_san` int(10) NOT NULL,
  `ngay_them` date NOT NULL,
  `trang_thai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `bai_viet`
--

INSERT INTO `bai_viet` (`id`, `hinh`, `ten_bai_viet`, `noi_dung`, `id_khach_san`, `ngay_them`, `trang_thai`) VALUES
(1, 'img-1.jpg', 'Giới thiệu khách sạn', 'Khi đến thăm Hà Nội, bạn sẽ cảm thấy như đang ở nhà tại Khách sạn Lucy Hotel, nơi có chất lượng tuyệt vời và dịch vụ chu đáo. Chỉ cách những hoạt động thú vị ở trung tâm thành phố khoảng 7 km. Không gian ấm cúng và gần Tòa nhà Keangnam Hà Nội, CUC Gallery, Trung tâm Hội nghị Quốc gia Việt Nam làm cho khách sạn này có một vẻ đẹp quyến rũ đặc biệt.\r\n\r\nHãy tận hưởng hết vô số dịch vụ và tiện nghi không gì sánh được ở khách sạn Hà Nội này. Khách hàng có thể tận hưởng các tiện nghi tại chỗ như dịch vụ phòng 24 giờ, miễn phí wifi tất cả các phòng, quầy lễ tân 24 giờ, giữ hành lý, wifi công cộng.\r\n\r\nKhách sạn rất chú ý đến việc trang bị đầy đủ tiện nghi để đạt được sự thoải mái và tiện lợi nhất. Trong một số phòng, khách hàng có thể thấy tivi màn hình phẳng, cafe hòa tan miễn phí, trà miễn phí, mền điện, nước uống chào đón miễn phí. Trong suốt một ngày, bạn có thể tận hưởng không gian thư giãn của sân golf (trong vòng 3 km). Dù cho lý do của bạn khi đến Hà Nội là gì đi nữa, Khách sạn Ping Hà Nội vẫn là một nơi tuyệt vời cho chuyến nghỉ mát vui vẻ và thú vị.', 1, '2021-06-25', 1),
(9, 'banner_5.jpg', 'abc', 'ádasd', 1, '2021-08-08', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `banner`
--

CREATE TABLE `banner` (
  `id` int(10) NOT NULL,
  `hinh` varchar(255) NOT NULL,
  `ten_banner` varchar(255) NOT NULL,
  `id_khach_san` int(10) NOT NULL,
  `trang_thai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `banner`
--

INSERT INTO `banner` (`id`, `hinh`, `ten_banner`, `id_khach_san`, `trang_thai`) VALUES
(2, 'banner_5.jpg', 'Đặt Phòng Cho Kỳ Nghỉ Gia Đình', 1, 1),
(3, 'banner_3.jpg', 'Làm Cho Kỳ Nghỉ Của Bạn Thoải Mái', 1, 1),
(7, 'banenr_4.jpg', 'Nơi Tốt Nhất Để Tận Hưởng Cuộc Sống Của Bạn', 1, 1),
(8, 'img-thumbs-4.jpg', 'Khách sạn', 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `dat_phong`
--

CREATE TABLE `dat_phong` (
  `id` int(10) NOT NULL,
  `ten_khach_hang` varchar(255) NOT NULL,
  `so_dien_thoai` varchar(11) NOT NULL,
  `CMND` varchar(12) NOT NULL,
  `email` varchar(255) NOT NULL,
  `id_phong` int(10) NOT NULL,
  `gia` float NOT NULL,
  `id_phan_tram_km` int(10) NOT NULL,
  `so_luong` int(10) NOT NULL,
  `ngay_den` date NOT NULL,
  `ngay_di` date NOT NULL,
  `nguoi_lon` int(10) NOT NULL,
  `tre_em` int(10) NOT NULL,
  `thanh_tien` float NOT NULL,
  `trang_thai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `dat_phong`
--

INSERT INTO `dat_phong` (`id`, `ten_khach_hang`, `so_dien_thoai`, `CMND`, `email`, `id_phong`, `gia`, `id_phan_tram_km`, `so_luong`, `ngay_den`, `ngay_di`, `nguoi_lon`, `tre_em`, `thanh_tien`, `trang_thai`) VALUES
(1, 'Tạ Văn Định', '12346', '65432', 'abc@gmail.com', 3, 150, 3, 1, '2021-07-17', '2021-07-18', 1, 1, 123456, 1),
(2, 'Nguyễn Thành Trung', '09876543', 'adasdsadasd', 'tadinh2000@gmail.com', 2, 1600000, 2, 2, '2021-08-07', '2021-08-08', 2, 3, 1280000, 1),
(3, 'Nguyễn Thành Trung123', '09876543', 'adasdsadasd', 'tadinh2000@gmail.com', 2, 1600000, 2, 2, '2021-08-07', '2021-08-08', 2, 3, 1280000, 1),
(4, 'ádsad', 'đasa', '0987654', 'tadinh2000@gmail.com', 4, 1800000, 1, 2, '2021-08-08', '2021-08-10', 2, 2, 1800000, 1),
(5, 'ádsad', 'đasa', '0987654', 'tadinh2000@gmail.com', 4, 1800000, 1, 2, '2021-08-08', '2021-08-10', 2, 2, 3600000, 1),
(6, 'Thịnh', '2222222', '111111111111', 'tadinh2000@gmail.com', 4, 1800000, 1, 2, '2021-08-08', '2021-08-10', 2, 0, 3600000, 1),
(7, 'Phạm Thị Hằng', '0987654321', '111111111111', 'tadinh2000@gmail.com', 6, 1700000, 2, 2, '2021-08-09', '2021-08-10', 1, 0, 2720000, 0),
(8, 'Phạm Thị Hằng 2', '09876543', '111111111111', 'tadinh2000@gmail.com', 6, 1700000, 2, 1, '2021-08-08', '2021-08-10', 1, 0, 2720000, 0),
(9, 'Phạm Thị Hằng 2345', '11111111111', '222222222', 'abc@gmail.com', 1, 123456, 2, 1, '2021-08-09', '2021-08-10', 1, 0, 98764.8, 1),
(10, 'Đặng Vũ Lưu', '444444444', '5555555555', '1811060685@hunre.edu.vn', 6, 1700000, 2, 3, '2021-08-10', '2021-08-11', 3, 0, 4080000, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `dich_vu_khach_san`
--

CREATE TABLE `dich_vu_khach_san` (
  `id` int(10) NOT NULL,
  `hinh` varchar(255) NOT NULL,
  `ten_dich_vu` varchar(255) NOT NULL,
  `thong_tin` text NOT NULL,
  `id_khach_san` int(10) NOT NULL,
  `trang_thai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `dich_vu_khach_san`
--

INSERT INTO `dich_vu_khach_san` (`id`, `hinh`, `ten_dich_vu`, `thong_tin`, `id_khach_san`, `trang_thai`) VALUES
(1, 'img-7.jpg', 'Coffee maker', 'Của hàng cafe', 1, 1),
(2, 'img-6.jpg', '25 inch or larger TV', 'abc', 1, 1),
(3, 'img-1.jpg', 'Cable/satellite TV channels', 'abcd', 1, 1),
(4, 'img-2.jpg', 'AM/FM clock radio', 'abcde', 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hom_thu`
--

CREATE TABLE `hom_thu` (
  `id` int(10) NOT NULL,
  `ten_khach_hang` varchar(255) NOT NULL,
  `so_dien_thoai` varchar(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `noi_dung` text NOT NULL,
  `trang_thai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `hom_thu`
--

INSERT INTO `hom_thu` (`id`, `ten_khach_hang`, `so_dien_thoai`, `email`, `noi_dung`, `trang_thai`) VALUES
(1, 'Tạ Văn Định', '0987654321', '1', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing', 1),
(2, 'Lương Thị Thịnh', '0123456', 'adasdasd', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one', 1),
(3, 'Nguyễn Thành Trung', '098765432', 'tadinh2000@gmail.com', 'abc', 1),
(4, 'Phạm Thị Hằng', '09876543', 'abc@gmail.com', 'tuyệt vời', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khach_san`
--

CREATE TABLE `khach_san` (
  `id` int(10) NOT NULL,
  `hinh` varchar(255) NOT NULL,
  `ten_khach_san` varchar(255) NOT NULL,
  `so_dien_thoai` varchar(11) NOT NULL,
  `dia_chi` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `trang_thai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `khach_san`
--

INSERT INTO `khach_san` (`id`, `hinh`, `ten_khach_san`, `so_dien_thoai`, `dia_chi`, `email`, `trang_thai`) VALUES
(1, 'Picture1.png', 'Lucy Hotel', '0379204876', '68 Xuân Thủy - Dịch Vọng Hậu - Cầu Giấy - Hà Nội', 'tadinh2000@gmail.com', 1),
(2, 'img-2.jpg', 'Lucy Hotel 1', '0987654342', 'Thành Phố Nam Định', 'rtyuiouytr@gmail.com', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khuyen_mai`
--

CREATE TABLE `khuyen_mai` (
  `id` int(10) NOT NULL,
  `hinh` varchar(255) NOT NULL,
  `ten_khuyen_mai` varchar(255) NOT NULL,
  `noi_dung` text NOT NULL,
  `phan_tram_km` float NOT NULL,
  `ngay_bat_dau` date NOT NULL,
  `ngay_ket_thuc` date NOT NULL,
  `id_khach_san` int(10) NOT NULL,
  `trang_thai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `khuyen_mai`
--

INSERT INTO `khuyen_mai` (`id`, `hinh`, `ten_khuyen_mai`, `noi_dung`, `phan_tram_km`, `ngay_bat_dau`, `ngay_ket_thuc`, `id_khach_san`, `trang_thai`) VALUES
(1, '1.jpg', 'giảm 50%', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled', 50, '2021-06-30', '2021-07-15', 1, 1),
(2, '2.jpg', 'giảm 20%', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled', 20, '2021-07-02', '2021-07-14', 1, 1),
(3, '5.jpg', 'Giảm 25%', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled', 25, '2021-07-02', '2021-07-17', 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loai_nguoi_dung`
--

CREATE TABLE `loai_nguoi_dung` (
  `id` int(10) NOT NULL,
  `ten_loai_nguoi_dung` varchar(255) NOT NULL,
  `id_khach_san` int(10) NOT NULL,
  `trang_thai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `loai_nguoi_dung`
--

INSERT INTO `loai_nguoi_dung` (`id`, `ten_loai_nguoi_dung`, `id_khach_san`, `trang_thai`) VALUES
(1, 'Quản lý', 2, 1),
(2, 'Nhân viên lễ tân', 1, 1),
(3, 'Nhân viên vệ sinh', 1, 1),
(4, 'Bảo vệ', 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loai_phong`
--

CREATE TABLE `loai_phong` (
  `id` int(10) NOT NULL,
  `hinh` varchar(255) NOT NULL,
  `ten_loai_phong` varchar(255) NOT NULL,
  `id_khach_san` int(10) NOT NULL,
  `thong_tin` text NOT NULL,
  `trang_thai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `loai_phong`
--

INSERT INTO `loai_phong` (`id`, `hinh`, `ten_loai_phong`, `id_khach_san`, `thong_tin`, `trang_thai`) VALUES
(1, 'banner_1.jpg', 'abc1', 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley ...', 1),
(2, 'img-1.jpg', 'Phòng VIP1', 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley ...', 1),
(3, 'img-2.jpg', 'Phòng tổng thống', 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley ...', 1),
(4, 'img-4.jpg', '1234', 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley ...', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nguoi_dung`
--

CREATE TABLE `nguoi_dung` (
  `id` int(10) NOT NULL,
  `ten_nguoi_dung` varchar(50) NOT NULL,
  `ngay_sinh` date NOT NULL,
  `gioi_tinh` tinyint(1) NOT NULL,
  `id_loai_nguoi_dung` int(10) NOT NULL,
  `so_dien_thoai` varchar(11) NOT NULL,
  `dia_chi` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ten_dang_nhap` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `trang_thai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `nguoi_dung`
--

INSERT INTO `nguoi_dung` (`id`, `ten_nguoi_dung`, `ngay_sinh`, `gioi_tinh`, `id_loai_nguoi_dung`, `so_dien_thoai`, `dia_chi`, `email`, `ten_dang_nhap`, `password`, `trang_thai`) VALUES
(1, 'Tạ Văn Định', '2000-08-30', 0, 1, '0379204876', 'Nam Định', 'tadinh2000@gmail.com', 'dinh', 'e10adc3949ba59abbe56e057f20f883e', 1),
(2, 'Phạm Thị Hằng', '2000-06-03', 1, 1, '0789852722', 'Nam Định', 'acvbnfg@gmail.com', 'phamhang', '030600', 1),
(3, 'Lương Thị Thịnh', '2000-07-01', 1, 2, '0789852722', 'Thường Tín', 'acvbnfg@gmail.com', 'thinh', 'e10adc3949ba59abbe56e057f20f883e', 1),
(4, 'Trương Thị Thu Trà', '2000-07-02', 1, 3, '0789852722', 'Mê Linh', 'acvbnfg@gmail.com', 'tra', '123456', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `phong`
--

CREATE TABLE `phong` (
  `id` int(10) NOT NULL,
  `hinh` varchar(255) NOT NULL,
  `ten_phong` varchar(255) NOT NULL,
  `id_loai_phong` int(10) NOT NULL,
  `gia` float NOT NULL,
  `so_nguoi` int(11) NOT NULL,
  `dien_tich` varchar(10) NOT NULL,
  `loai_giuong` varchar(255) NOT NULL,
  `thong_tin` text NOT NULL,
  `trang_thai` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `phong`
--

INSERT INTO `phong` (`id`, `hinh`, `ten_phong`, `id_loai_phong`, `gia`, `so_nguoi`, `dien_tich`, `loai_giuong`, `thong_tin`, `trang_thai`) VALUES
(1, 'img-1.jpg', 'phòng tổng thống', 2, 123456, 3, '15m2', 'giường đôi', 'Located in the heart of Aspen with a unique blend of contemporary luxury and historic heritage, deluxe accommodations, superb amenities, genuine hospitality and dedicated service for an elevated experience in the Rocky Mountains.', 2),
(2, 'img-2.jpg', 'phòng thường', 1, 1600000, 2, '15m2', 'giường đơn', 'Located in the heart of Aspen with a unique blend of contemporary luxury and historic heritage, deluxe accommodations, superb amenities, genuine hospitality and dedicated service for an elevated experience in the Rocky Mountains.', 2),
(3, 'img-1.jpg', 'Phòng VIP', 1, 1700000, 2, '15m2', 'giường đơn', 'Located in the heart of Aspen with a unique blend of contemporary luxury and historic heritage, deluxe accommodations, superb amenities, genuine hospitality and dedicated service for an elevated experience in the Rocky Mountains.', 2),
(4, 'img-2.jpg', 'Phòng scanhand', 1, 1800000, 3, '15m2', 'giường đôi', 'Located in the heart of Aspen with a unique blend of contemporary luxury and historic heritage, deluxe accommodations, superb amenities, genuine hospitality and dedicated service for an elevated experience in the Rocky Mountains.', 2),
(6, 'img-thumbs-1.jpg', 'abc', 3, 1700000, 2, '15m2', 'giường đơn', 'Located in the heart of Aspen with a unique blend of contemporary luxury and historic heritage, deluxe accommodations, superb amenities, genuine hospitality and dedicated service for an elevated experience in the Rocky Mountains.', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tin_tuc`
--

CREATE TABLE `tin_tuc` (
  `id` int(10) NOT NULL,
  `hinh` varchar(255) NOT NULL,
  `ten_tin_tuc` varchar(255) NOT NULL,
  `id_khach_san` int(10) NOT NULL,
  `noi_dung` text NOT NULL,
  `ngay_them` date NOT NULL,
  `trang_thai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tin_tuc`
--

INSERT INTO `tin_tuc` (`id`, `hinh`, `ten_tin_tuc`, `id_khach_san`, `noi_dung`, `ngay_them`, `trang_thai`) VALUES
(1, 'banner_2.jpg', 'abc', 1, 'abc', '2021-08-07', 1),
(2, 'img-9.jpg', 'dịnh', 1, 'Phạt thanh niên sĩ gái sáng nay vì hành vi liên tục \"hỏi thăm sức khỏe\" chú công an 2 triệu đồng', '2021-08-07', 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bai_viet`
--
ALTER TABLE `bai_viet`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `dat_phong`
--
ALTER TABLE `dat_phong`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `dich_vu_khach_san`
--
ALTER TABLE `dich_vu_khach_san`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `hom_thu`
--
ALTER TABLE `hom_thu`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `khach_san`
--
ALTER TABLE `khach_san`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `khuyen_mai`
--
ALTER TABLE `khuyen_mai`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `loai_nguoi_dung`
--
ALTER TABLE `loai_nguoi_dung`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `loai_phong`
--
ALTER TABLE `loai_phong`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `phong`
--
ALTER TABLE `phong`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tin_tuc`
--
ALTER TABLE `tin_tuc`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bai_viet`
--
ALTER TABLE `bai_viet`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `dat_phong`
--
ALTER TABLE `dat_phong`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `dich_vu_khach_san`
--
ALTER TABLE `dich_vu_khach_san`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `hom_thu`
--
ALTER TABLE `hom_thu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `khach_san`
--
ALTER TABLE `khach_san`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `khuyen_mai`
--
ALTER TABLE `khuyen_mai`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `loai_nguoi_dung`
--
ALTER TABLE `loai_nguoi_dung`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `loai_phong`
--
ALTER TABLE `loai_phong`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `phong`
--
ALTER TABLE `phong`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `tin_tuc`
--
ALTER TABLE `tin_tuc`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
